FROM ubuntu;22.04

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl wget unzip jq

RUN wget https://releases.hashicorp.com/terraform/0.14.10/terraform_0.14.10_linux_amd.zip && \
    unzip terraform_0.14.10_linux_amd64.zip && \
    mv terraform /usr/local/bin && \
    rm terraform_o.14.10_linux_amd64.zip

WORKDIR /app

COPY . .
RUN chmod +x terraform
RUN cat main.terraform
RUN terraform init
RUN terraform plan -out=tfplan

ENTRYPOINT [""]
CMD ["/bin/bash"]
